<?php

require_once __DIR__.'/../vendor/autoload.php';

define('APP_BASE_URL', 'http://'.$_SERVER['HTTP_HOST'].substr($_SERVER['SCRIPT_NAME'], 0, strrpos($_SERVER['SCRIPT_NAME'], '/')).'/index.php');
define('APP_BASE_ASSETS', 'http://'.$_SERVER['HTTP_HOST'].substr($_SERVER['SCRIPT_NAME'], 0, strrpos($_SERVER['SCRIPT_NAME'], '/')));
define('APP_BASE_PATH', realpath(__DIR__.'/..'));

$app = new Silex\Application(
    array(
        'db.options' => array(
            'driver' => 'pdo_sqlite',
            //'driver' => 'pdo_mysql',
            'path'   => APP_BASE_PATH.'/var/db/blog.db',
            //'dbname' => 'blog',
        ),
    )
);

$app->register(new Silex\Provider\UrlGeneratorServiceProvider());
$app->register(new Silex\Provider\SessionServiceProvider());
$app->register(new Silex\Provider\DoctrineServiceProvider());

$app->register(
    new Silex\Provider\SecurityServiceProvider(),
    array(
        'security.firewalls' => array(
            'main' => array(
                'pattern'   => '^/',
                'anonymous' => true,
                'form'      => array(
                    'login_path' => '/login',
                    'check_path' => '/admin/login_check',
                ),
                'logout'    => array(
                    'logout_path'        => '/admin/logout',
                    'invalidate_session' => false,
                ),
                'users'     => $app->share(
                    function () use ($app) {
                        return new MyBlog\Security\Provider\UserProvider($app['db']);
                    }
                ),
            ),
        ),
    )
);

$app['security.access_rules'] = array(
    array('^/', 'IS_AUTHENTICATED_ANONYMOUSLY'),
);

$app['security.encoder.digest'] = $app->share(
    function () {
        return new \Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder('md5', false, 1);
    }
);

$app->register(
    new ThisPageCannotBeFound\Silex\Provider\PhpTemplatingServiceProvider(),
    array(
        'templating.paths' => APP_BASE_PATH.'/app/Resources/views',
    )
);

$app['helper.url'] = $app->share(
    function () {
        return new \MyBlog\Helper\UrlHelper();
    }
);