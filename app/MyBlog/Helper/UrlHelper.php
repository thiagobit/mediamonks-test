<?php

namespace MyBlog\Helper;

/**
 * Class UrlHelper
 *
 * @package MyBlog\Helper
 */
class UrlHelper
{
    /**
     * @return string
     */
    public function getCurrentURL()
    {
        return 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    }

    /**
     * @return mixed
     */
    public function getCurrentPath()
    {
        return str_replace($_SERVER['SCRIPT_NAME'], null, $_SERVER['REQUEST_URI']);
    }

    /**
     * @param $path
     * @param bool $exactMatch
     * @return bool
     */
    public function isPath($path, $exactMatch = false)
    {
        if ($exactMatch) {
            return $this->getCurrentPath() !== $path;
        }

        return strpos($this->getCurrentPath(), $path) === false;
    }
}