<?php

namespace MyBlog\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminController
 *
 * @package MyBlog\Controller
 */
class AdminController
{
    /**
     * @param Application $app
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function indexAction(Application $app)
    {
        if($app['security.authorization_checker']->isGranted('ROLE_ADMIN'))
        {
            return $app->redirect($app['url_generator']->generate('admin_blog_post_index'));
        }

        return $app->redirect($app['url_generator']->generate('login'));
    }

    /**
     * @param Application $app
     * @return mixed
     */
    public function blogIndexAction(Application $app)
    {
        $sql = 'SELECT p.id, p.title, p.body, p.created, (SELECT COUNT(1) FROM comment c WHERE c.blog_id = p.id) as numComments
                FROM post p
                ORDER BY id DESC';

        $posts = $app['db']->fetchAll($sql);

        return $app['templating']->render(
            'admin/blog_index.php',
            array('posts' => $posts)
        );
    }

    /**
     * @param Request $request
     * @param Application $app
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function blogCreateAction(Request $request, Application $app)
    {
        $date = new \DateTime();

        $post = array(
            'id'      => null,
            'title'   => null,
            'body'    => null,
            'created' => $date->format('r'),
        );

        if($request->getMethod() == 'POST')
        {
            $post['title'] = filter_var($request->get('title'), FILTER_SANITIZE_MAGIC_QUOTES);
            $post['body']  = filter_var($request->get('body'),  FILTER_SANITIZE_MAGIC_QUOTES);

            $app['db']->insert('post', $post);

            return $app->redirect($app['url_generator']->generate('admin_blog_post_index'));
        }

        return $app['templating']->render(
            'admin/blog_edit.php',
            array('post' => $post)
        );
    }

    /**
     * @param Request $request
     * @param Application $app
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function blogEditAction(Request $request, Application $app, $id)
    {
        $id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);

        $post     = $app['db']->fetchAssoc('SELECT * FROM post    WHERE id      = ?', array($id));
        $comments = $app['db']->fetchAll('  SELECT * FROM comment WHERE blog_id = ?', array($id));

        if($request->getMethod() == 'POST')
        {
            $post['title'] = filter_var($request->get('title'), FILTER_SANITIZE_MAGIC_QUOTES);
            $post['body']  = filter_var($request->get('body'),  FILTER_SANITIZE_MAGIC_QUOTES);

            $app['db']->update('post', $post, array('id' => $id));

            return $app->redirect($app['url_generator']->generate('admin_blog_post_index'));
        }

        return $app['templating']->render(
            'admin/blog_edit.php',
            array(
                'post'     => $post,
                'comments' => $comments
            )
        );
    }

    /**
     * @param Application $app
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function blogDeleteAction(Application $app, $id)
    {
        $id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);

        $app['db']->delete('post', array('id' => $id));

        return $app->redirect($app['url_generator']->generate('admin_blog_post_index'));
    }

    /**
     * @param Request $request
     * @param Application $app
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function commentEditAction(Request $request, Application $app, $id)
    {
        $id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);

        $comment = $app['db']->fetchAssoc('SELECT * FROM comment WHERE id = ?', array($id));

        if($request->getMethod() == 'POST')
        {
            $comment['name']    = filter_var($request->get('name'),    FILTER_SANITIZE_MAGIC_QUOTES);
            $comment['email']   = filter_var($request->get('email'),   FILTER_SANITIZE_MAGIC_QUOTES);
            $comment['comment'] = filter_var($request->get('comment'), FILTER_SANITIZE_MAGIC_QUOTES);

            $app['db']->update('comment', $comment, array('id' => $id));

            return $app->redirect(
                $app['url_generator']->generate(
                    'admin_blog_post_edit',
                    array('id' => $comment['blog_id'])
                )
            );
        }

        return $app['templating']->render(
            'admin/comment_edit.php',
            array('comment' => $comment)
        );
    }

    /**
     * @param Application $app
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function commentDeleteAction(Application $app, $id)
    {
        $id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);

        $comment = $app['db']->fetchAssoc('SELECT blog_id FROM comment WHERE id = ?', array($id));
        $blogId  = $comment['blog_id'];

        $app['db']->delete('comment', array('id' => $id));

        return $app->redirect(
            $app['url_generator']->generate(
                'admin_blog_post_edit',
                array('id' => $blogId)
            )
        );
    }
}