<?php

namespace MyBlog\Controller;

use Silex\Application;

/**
 * Class AuthController
 *
 * @package MyBlog\Controller
 */
class AuthController
{
    /**
     * @param Application $app
     * @return mixed
     */
    public function loginAction(Application $app)
    {
        return $app['templating']->render('login.php');
    }
}