<?php

namespace MyBlog\Controller;

use Silex\Application;

/**
 * Class IndexController
 *
 * @package MyBlog\Controller
 */
class IndexController
{
    /**
     * @param Application $app
     * @return mixed
     */
    public function indexAction(Application $app)
    {
        return $app->redirect($app['url_generator']->generate('homepage'));
    }
}