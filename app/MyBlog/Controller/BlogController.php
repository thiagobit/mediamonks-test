<?php

namespace MyBlog\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class BlogController
 *
 * @package MyBlog\Controller
 */
class BlogController
{
    /**
     * @param Application $app
     * @return mixed
     */
    public function indexAction(Application $app)
    {
        $posts = $app['db']->fetchAll('SELECT * FROM post');

        return $app['templating']->render(
            'blog.php',
            array('posts' => $posts)
        );
    }

    /**
     * @param Application $app
     * @param $id
     * @return mixed
     */
    public function blogPostAction(Application $app, $id)
    {
        $id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);

        $post     = $app['db']->fetchAssoc('SELECT * FROM post    WHERE id      = ?', array($id));
        $comments = $app['db']->fetchAll('  SELECT * FROM comment WHERE blog_id = ?', array($id));

        return $app['templating']->render(
            'blog_post.php',
            array(
                'post'     => $post,
                'comments' => $comments
            )
        );
    }

    /**
     * @param Application $app
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function blogPostCommentAction(Application $app, Request $request, $id)
    {
        $id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);

        $date = new \DateTime();

        $data = array(
            'blog_id' => $id,
            'name'    => filter_var($request->get('name'),    FILTER_SANITIZE_MAGIC_QUOTES),
            'email'   => filter_var($request->get('email'),   FILTER_SANITIZE_MAGIC_QUOTES),
            'comment' => filter_var($request->get('comment'), FILTER_SANITIZE_MAGIC_QUOTES),
            'created' => $date->format('r'),
        );

        $fields = implode(',', array_keys($data));

        $values = implode(
            "','",
            array_map(
                function ($value) {
                    return str_replace("'", "''", $value);
                },
                $data
            )
        );

        $app['db']->executeQuery("INSERT INTO comment (".$fields.") VALUES ('".$values."')");

        return $app->redirect(
            $app['url_generator']->generate(
                'blog_post',
                array('id' => $id)
            )
        );
    }
}