<?php include __DIR__.'/header.php'; ?>

<?php foreach ($posts as $post): ?>

    <?php

    $sql = 'SELECT * FROM comment WHERE blog_id = '.$post['id'];
    $comments = $app['db']->fetchAll($sql);

    $post['numComments'] = 0;

    foreach ($comments as $comment) {
        $post['numComments']++;
    }

    ?>

    <div class="panel panel-default">

        <div class="panel-heading">
            <span class="pull-right"><small><?= date('j F Y H:i', strtotime($post['created'])) ?></small></span>
            <a href="<?= APP_BASE_URL.'/blog/'.$post['id'] ?>"><h4><?= $post['title'] ?></h4></a>
        </div>

        <div class="panel-body">
            <p>
                <?= $post['body'] ?>
            </p>
        </div>

        <div class="panel-footer">
            <a href="<?= APP_BASE_URL.'/blog/'.$post['id'] ?>" class="btn btn-default">Read more
                (<?= $post['numComments'] ?> comments)</a>
        </div>

    </div>

<?php endforeach ?>

<?php include __DIR__.'/footer.php'; ?>