<?php include __DIR__.'/header.php'; ?>

    <div class="panel panel-default">

        <div class="panel-heading">
            <h4><?= $post['title'] ?></h4>
        </div>

        <div class="panel-body">
            <p>
                <?= $post['body'] ?>
            </p>
            <span class="pull-right"><small><?= date('j F Y H:i', strtotime($post['created'])) ?></small></span>
        </div>
    </div>

    <div class="panel panel-default">

        <div class="panel-heading">
            <h4>Comments (<?= count($comments) ?>)</h4>
        </div>

        <ul class="list-group">
            <?php foreach ($comments as $comment): ?>
                <li class="list-group-item">
                    <blockquote>
                        <p><?= $comment['comment'] ?></p>
                        <footer><cite title="Source Title"><?= $comment['name'] ?></cite> on <?= date(
                                'j F Y H:i',
                                strtotime($comment['created'])
                            ) ?></footer>
                    </blockquote>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>

    <div class="panel panel-default">

        <div class="panel-heading">
            <h4>Write comment</h4>
        </div>

        <div class="panel-body">

            <form action="<?= APP_BASE_URL.'/blog/'.$post['id'].'/comment' ?>" method="POST" id="comment-form">

                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" name="name" id="name"/>
                </div>

                <div class="form-group">
                    <label for="email">E-mail</label>
                    <input type="email" class="form-control" name="email" id="email"/>
                </div>

                <div class="form-group">
                    <label for="comment">Comment</label>
                    <textarea id="comment" class="form-control" name="comment"></textarea>
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>

            </form>

            <script type="text/javascript">

                var validateForm = function (e) {
                    if (this.name.value == '') {
                        alert('Name is required');
                        this.name.focus();
                        e.preventDefault();
                    } else if (this.email.value == '') {
                        alert('E-mail is required');
                        this.email.focus();
                        e.preventDefault();
                    } else if (this.comment.value == '') {
                        alert('Comment is required');
                        this.comment.focus();
                        e.preventDefault();
                    }
                };

                window.addEventListener('load', function () {
                    var form = document.getElementById('comment-form');
                    form.addEventListener('submit', validateForm);
                });

            </script>

        </div>

    </div>

<?php include __DIR__.'/footer.php'; ?>