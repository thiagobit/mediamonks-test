<?php include __DIR__.'/header.php'; ?>

    <div class="panel panel-default">

        <div class="panel-heading">
            <h4>Login</h4>
        </div>

        <div class="panel-body">

            <form action="<?= APP_BASE_URL.'/admin/login_check' ?>" method="POST" id="login-form">

                <?php if ($app['security.last_error']($app['request']) !== null): ?>
                    <p class="alert alert-danger">
                        <?= $app['security.last_error']($app['request']) ?>
                    </p>
                <?php endif; ?>

                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" class="form-control" name="_username" id="username"
                           value="<?= $app['session']->get('_security.last_username') ?>"/>
                </div>

                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" name="_password" id="password"/>
                </div>

                <button type="submit" class="btn btn-primary">Login</button>

            </form>

            <script type="text/javascript">

                var validateForm = function (e) {
                    if (this.username.value == '') {
                        alert('Username is required');
                        this.username.focus();
                        e.preventDefault();
                    } else if (this.password.value == '') {
                        alert('Password is required');
                        this.password.focus();
                        e.preventDefault();
                    }
                };

                window.addEventListener('load', function () {
                    var form = document.getElementById('login-form');
                    form.addEventListener('submit', validateForm);
                });

            </script>
        </div>

    </div>

<?php include __DIR__.'/footer.php'; ?>