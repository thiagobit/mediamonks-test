<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>My blog</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="<?= APP_BASE_ASSETS ?>/css/bootstrap.min.css">

    <script>

        window.addEventListener('load', function () {

            var btnLogout = document.getElementsByClassName('logout');
            if (btnLogout.length) {
                btnLogout[0].addEventListener('click', function (e) {
                    if (!confirm('Are you sure?')) {
                        e.preventDefault();
                    }
                });
            }

            var deleteButtons = document.getElementsByClassName('delete');
            for (var i = 0; i < deleteButtons.length; i++) {
                deleteButtons[i].addEventListener('click', function (e) {
                    if (!confirm('Are you sure?')) {
                        e.preventDefault();
                    }
                });
            }
        });

    </script>

</head>
<body>

<div class="container">

    <div class="row">
        <div class="col-md-12">

            <header>

                <div class="page-header">
                    <h1>My Blog is better
                        <small>than yours</small>
                    </h1>
                </div>

                <ul class="nav nav-tabs">

                    <li<?= $app['helper.url']->isPath('/blog', true) ? null : ' class="active"' ?>><a
                            role="presentation" href="<?= APP_BASE_URL ?>">Blog</a></li>

                    <?php if ($app['security.authorization_checker']->isGranted('ROLE_ADMIN')): ?>
                        <li<?= $app['helper.url']->isPath('/admin') ? null : ' class="active"' ?>><a role="presentation"
                                                                                                     href="<?= APP_BASE_URL ?>/admin">Admin
                                Panel</a></li>
                        <li><a role="presentation" href="<?= APP_BASE_URL ?>/admin/logout" class="logout">Logout</a>
                        </li>
                    <?php else: ?>
                        <li<?= $app['helper.url']->isPath('/login') ? null : ' class="active"' ?>><a role="presentation"
                                                                                                     href="<?= APP_BASE_URL ?>/login">Login</a>
                        </li>
                    <?php endif; ?>

            </header>

            <div class="row">
                <div class="col-md-12">&nbsp;</div>
            </div>
