<?php include __DIR__.'/../header.php'; ?>

    <div class="panel panel-default">

        <div class="panel-heading">
            <?php if ($comment['id'] !== null): ?>
                <h4>Edit comment</h4>
            <?php else: ?>
                <h4>Create comment</h4>
            <?php endif; ?>
        </div>

        <div class="panel-body">

            <form action="<?= $app['helper.url']->getCurrentUrl() ?>" method="POST" id="comment-form">

                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" name="name" id="name" value="<?= $comment['name'] ?>"/>
                </div>

                <div class="form-group">
                    <label for="email">E-mail</label>
                    <input type="text" class="form-control" name="email" id="email" value="<?= $comment['email'] ?>"/>
                </div>

                <div class="form-group">
                    <label for="comment">Comment</label>
                    <textarea class="form-control" name="comment" id="comment"
                              rows="4"><?= $comment['comment'] ?></textarea>
                </div>

                <a href="<?= APP_BASE_URL.'/admin/blog/'.$comment['blog_id'].'/edit' ?>"
                   class="btn btn-default">Cancel</a>
                <button type="submit" class="btn btn-primary">Submit</button>

            </form>

            <script type="text/javascript">

                var validateForm = function (e) {
                    if (this.name.value == '') {
                        alert('Name is required');
                        this.name.focus();
                        e.preventDefault();
                    } else if (this.email.value == '') {
                        alert('E-mail is required');
                        this.email.focus();
                        e.preventDefault();
                    } else if (this.body.value == '') {
                        alert('Body is required');
                        this.body.focus();
                        e.preventDefault();
                    }
                };

                window.addEventListener('load', function () {
                    var form = document.getElementById('comment-form');
                    form.addEventListener('submit', validateForm);
                });

            </script>

        </div>
    </div>

<?php include __DIR__.'/../footer.php'; ?>