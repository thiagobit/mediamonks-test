<?php include __DIR__.'/../header.php'; ?>

    <div class="panel panel-default">

        <div class="panel-heading">
            <a href="<?= APP_BASE_URL.'/admin/blog/create' ?>" class="btn btn-primary pull-right">Create new</a>
            <h4>Blog Administration Panel</h4>
        </div>

        <div class="panel-body">
            <table class="table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Date</th>
                    <th>Comments</th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($posts as $post): ?>
                    <tr>
                        <td><?= $post['id'] ?></td>
                        <td><?= $post['title'] ?></td>
                        <td><?= date('d-m-Y H:i', strtotime($post['created'])) ?></td>
                        <td><?= $post['numComments'] ?></td>
                        <td class="text-right">
                            <a href="<?= APP_BASE_URL.'/admin/blog/'.$post['id'] ?>/edit"
                               class="btn btn-primary">Edit</a>
                            <a href="<?= APP_BASE_URL.'/admin/blog/'.$post['id'] ?>/delete"
                               class="btn btn-warning delete">Delete</a>
                        </td>
                    </tr>
                <?php endforeach ?>
                </tbody>
            </table>
        </div>

    </div>

<?php include __DIR__.'/../footer.php'; ?>