<?php include __DIR__.'/../header.php'; ?>

    <div class="panel panel-default">

        <div class="panel-heading">
            <?php if ($post['id'] !== null): ?>
                <h4>Edit blog</h4>
            <?php else: ?>
                <h4>Create blog</h4>
            <?php endif; ?>
        </div>

        <div class="panel-body">

            <form action="<?= $app['helper.url']->getCurrentUrl() ?>" method="POST" id="blog-form">

                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" name="title" id="title" value="<?= $post['title'] ?>"/>
                </div>

                <div class="form-group">
                    <label for="title">Body</label>
                    <textarea class="form-control" name="body" id="body" rows="4"><?= $post['body'] ?></textarea>
                </div>

                <a href="<?= APP_BASE_URL.'/admin/blog' ?>" class="btn btn-default">Cancel</a>
                <button type="submit" class="btn btn-primary">Submit</button>

            </form>

            <script type="text/javascript">

                var validateForm = function (e) {
                    if (this.title.value == '') {
                        alert('Title is required');
                        this.title.focus();
                        e.preventDefault();
                    } else if (this.body.value == '') {
                        alert('Body is required');
                        this.body.focus();
                        e.preventDefault();
                    }
                };

                window.addEventListener('load', function () {
                    var form = document.getElementById('blog-form');
                    form.addEventListener('submit', validateForm);
                });

            </script>

        </div>
    </div>

    <?php if(isset($comments)): ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>Comments</h4>
        </div>

        <div class="panel-body">

            <div class="panel-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Date</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($comments as $comment): ?>
                        <tr>
                            <td><?= $comment['id'] ?></td>
                            <td><?= $comment['name'] ?></td>
                            <td><?= $comment['email'] ?></td>
                            <td><?= $comment['comment'] ?></td>
                            <td><?= date('d-m-Y H:i', strtotime($post['created'])) ?></td>
                            <td class="text-right">
                                <a href="<?= APP_BASE_URL.'/admin/comment/'.$comment['id'] ?>/edit"
                                   class="btn btn-primary">Edit</a>
                                <a href="<?= APP_BASE_URL.'/admin/comment/'.$comment['id'] ?>/delete"
                                   class="btn btn-warning delete">Delete</a>
                            </td>
                        </tr>
                    <?php endforeach ?>
                    </tbody>
                </table>
            </div>

        </div>

    </div>
    <?php endif; ?>

<?php include __DIR__.'/../footer.php'; ?>