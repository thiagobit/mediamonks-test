<?php

require_once '../app/bootstrap.php';

$controllerNamespace = 'MyBlog\\Controller\\';

$app->get('/', $controllerNamespace.'IndexController::indexAction')->bind('index');
$app->get('/blog', $controllerNamespace.'BlogController::indexAction')->bind('homepage');
$app->get('/blog/{id}', $controllerNamespace.'BlogController::blogPostAction')->bind('blog_post');
$app->match('/blog/{id}/comment', $controllerNamespace.'BlogController::blogPostCommentAction')->bind('blog_comment');

$app->get('/login', $controllerNamespace.'AuthController::loginAction')->bind('login');
$app->get('/admin', $controllerNamespace.'AdminController::indexAction')->bind('admin_index');
$app->get('/admin/blog', $controllerNamespace.'AdminController::blogIndexAction')->bind('admin_blog_post_index');
$app->match('/admin/blog/create', $controllerNamespace.'AdminController::blogCreateAction')->bind('admin_blog_post_create');
$app->match('/admin/blog/{id}/edit', $controllerNamespace.'AdminController::blogEditAction')->bind('admin_blog_post_edit');
$app->match('/admin/blog/{id}/delete', $controllerNamespace.'AdminController::blogDeleteAction')->bind('admin_blog_post_delete');
$app->match('/admin/comment/{id}/edit', $controllerNamespace.'AdminController::commentEditAction')->bind('admin_comment_edit');
$app->match('/admin/comment/{id}/delete', $controllerNamespace.'AdminController::commentDeleteAction')->bind('admin_comment_delete');

$app->run();